package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

import org.w3c.dom.UserDataHandler;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {
        Random r = new Random();
        int random = r.nextInt(rpsChoices.size());
        String compMove = rpsChoices.get(random);

        System.out.println("Let's play round " + roundCounter);
        String userMove;

            while (true) {
                userMove = readInput("Your choice (Rock/Paper/Scissors)?");
                if (userMove.equals("rock") || userMove.equals("paper") || userMove.equals("scissors")) {
                    break;
                }
            System.out.println("I do not understand " + userMove + ". Could you try again?");
            }
        
            if (userMove.equals(compMove)) {
                System.out.println("Human chose " + userMove + ", computer chose " + compMove + ". It`s a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else if (userMove.equals("rock")) {
                if (compMove.equals("paper")) {
                    System.out.println("Human chose rock, computer chose paper. Computer wins!");
                    computerScore = computerScore + 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (compMove.equals("scissors")) {
                    System.out.println("Human chose rock, computer chose scissors. Human wins!");
                    humanScore = humanScore + 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }

            else if (userMove.equals("paper")) {
                if (compMove.equals("rock")) {
                    System.out.println("Human chose paper, computer chose rock. Human wins!");
                    humanScore = humanScore + 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (compMove.equals("scissors")) {
                    System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                    computerScore = computerScore + 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            
            }

            else if (userMove.equals("scissors")) {
                if (compMove.equals("rock")) {
                    System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                    computerScore = computerScore + 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (compMove.equals("paper")) {
                    System.out.println("Human chose scissors, computer chose paper. Human wins!");
                    humanScore = humanScore+ 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }              
            }
            
            String playAgain = readInput("Do you wish to continue playing? (y/n)?");
            if (!playAgain.equals("y")) {
                break;
            }
            roundCounter = roundCounter + 1;
    }
    System.out.println("Bye bye :)");
    }

    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
